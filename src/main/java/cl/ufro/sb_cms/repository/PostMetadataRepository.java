package cl.ufro.sb_cms.repository;

import cl.ufro.sb_cms.model.PostMetadata;
import cl.ufro.sb_cms.repository.interfaces.PostMetadataRep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.awt.print.Pageable;
import java.util.List;

@Repository
public class PostMetadataRepository implements PostMetadataRep {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public boolean save(PostMetadata object) {
        return false;
    }

    @Override
    public boolean update(PostMetadata object) {
        return false;
    }

    @Override
    public List<PostMetadata> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public PostMetadata findById(int id) {
        return null;
    }
}
