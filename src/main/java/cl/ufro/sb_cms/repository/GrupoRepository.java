package cl.ufro.sb_cms.repository;

import cl.ufro.sb_cms.model.Grupo;
import cl.ufro.sb_cms.repository.interfaces.GrupoRep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.awt.print.Pageable;
import java.util.List;

@Repository
public class GrupoRepository implements GrupoRep {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public boolean save(Grupo object) {
        return false;
    }

    @Override
    public boolean update(Grupo object) {
        return false;
    }

    @Override
    public List<Grupo> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public Grupo findById(int id) {
        return null;
    }
}
