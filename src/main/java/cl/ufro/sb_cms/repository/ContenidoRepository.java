package cl.ufro.sb_cms.repository;

import cl.ufro.sb_cms.model.Comentario;
import cl.ufro.sb_cms.model.Contenido;
import cl.ufro.sb_cms.repository.interfaces.ContenidoRep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.awt.print.Pageable;
import java.util.List;

@Repository
public class ContenidoRepository implements ContenidoRep {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public boolean save(Contenido contenido) {
        String sql = "INSERT INTO Contenido (Contenido, IdPost, Tipo) VALUES (?, ?, ?)";
        try{
            jdbcTemplate.update(
                    sql,
                    contenido.getContenido(), contenido.getIdPost(), contenido.getTipo());
            return true;
        }catch (Exception e){
            return false;
        }
    }

    @Override
    public boolean update(Contenido contenido) {
        if (contenido.getIdContenido() > 0) {
            String sql = "UPDATE Comentario SET Contenido = ?, Tipo = ?" +
                    "WHERE IdContenido = ? ";
            jdbcTemplate.update(sql,
                    contenido.getContenido(), contenido.getTipo(), contenido.getIdContenido());
            return true;
        }
        return false;
    }

    @Override
    public List<Contenido> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public Contenido findById(int id) {
        return null;
    }
}
