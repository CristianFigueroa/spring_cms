package cl.ufro.sb_cms.repository;

import cl.ufro.sb_cms.model.Categoria;
import cl.ufro.sb_cms.repository.interfaces.CategoriaRep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.awt.print.Pageable;
import java.sql.PreparedStatement;
import java.util.List;

@Repository
public class CategoriaRepository implements CategoriaRep {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private PreparedStatement sql = null;


    @Override
    public boolean save(Categoria categoria) {
        String sql = "INSERT INTO Categoria (Nombre, Descripcion, CategoriaSuperior) VALUES (?, ?, ?)";
        try{
            jdbcTemplate.update(
                    sql,
                    categoria.getNombre(),categoria.getDescripcion(),categoria.getCategoriaSuperior());
            return true;
        }catch (Exception e){
            return false;
        }
    }

    @Override
    public boolean update(Categoria categoria) {
        if(categoria.getCategoriaSuperior() != 0) {
            String sql = "UPDATE Categoria SET Nombre = ?, Descripcion = ?, CategoriaSuperior = ? " +
                    "WHERE IdCategoria = ? ";
            jdbcTemplate.update(sql,
                    categoria.getNombre(), categoria.getDescripcion(), categoria.getCategoriaSuperior(), categoria.getIdCategoria());
            return true;
        }
        return false;
    }

    @Override
    public List<Categoria> findAll(Pageable pageable) {
        return null;
    }


    @Override
    public Categoria findById(int id) {
        return null;
    }

}
