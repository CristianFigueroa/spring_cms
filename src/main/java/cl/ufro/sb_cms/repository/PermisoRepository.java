package cl.ufro.sb_cms.repository;

import cl.ufro.sb_cms.model.Permiso;
import cl.ufro.sb_cms.repository.interfaces.PermisoRep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.awt.print.Pageable;
import java.util.List;

@Repository
public class PermisoRepository implements PermisoRep {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public boolean save(Permiso object) {
        return false;
    }

    @Override
    public boolean update(Permiso object) {
        return false;
    }

    @Override
    public List<Permiso> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public Permiso findById(int id) {
        return null;
    }
}
