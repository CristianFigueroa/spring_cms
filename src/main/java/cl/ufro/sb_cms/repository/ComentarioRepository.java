package cl.ufro.sb_cms.repository;

import cl.ufro.sb_cms.model.Categoria;
import cl.ufro.sb_cms.model.Comentario;
import cl.ufro.sb_cms.repository.interfaces.ComentarioRep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.awt.print.Pageable;
import java.util.List;

@Repository
public class ComentarioRepository implements ComentarioRep {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public boolean save(Comentario comentario) {
        String sql = "INSERT INTO Comentario (Comentario, IdPost, IdUsuario, Respuesta) VALUES (?, ?, ?, ?)";
        try{
            jdbcTemplate.update(
                    sql,
                    comentario.getComentario(),comentario.getIdPost(),comentario.getIdUsuario(),comentario.getRespuesta());
            return true;
        }catch (Exception e){
            return false;
        }
    }

    @Override
    public boolean update(Comentario comentario) {
        if (comentario.getIdComentario() > 0) {
            String sql = "UPDATE Comentario SET Comentario = ?, IdPost = ?, IdUsuario = ?, Respuesta = ?" +
                    "WHERE IdComentario = ? ";
            jdbcTemplate.update(sql,
                    comentario.getComentario(), comentario.getIdPost(), comentario.getIdUsuario(), comentario.getRespuesta(),comentario.getIdComentario());
            return true;
        }
        return false;
    }

    @Override
    public List<Comentario> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public Comentario findById(int id) {
        return null;
    }
}
