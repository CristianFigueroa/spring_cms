package cl.ufro.sb_cms.repository.interfaces;

import cl.ufro.sb_cms.model.Categoria;

public interface CategoriaRep extends BaseRep<Categoria>{

}
