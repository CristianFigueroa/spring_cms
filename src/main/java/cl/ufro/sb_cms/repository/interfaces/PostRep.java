package cl.ufro.sb_cms.repository.interfaces;

import cl.ufro.sb_cms.model.Post;

public interface PostRep extends BaseRep<Post>{
}
