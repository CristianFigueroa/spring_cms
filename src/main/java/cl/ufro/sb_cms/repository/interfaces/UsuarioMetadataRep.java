package cl.ufro.sb_cms.repository.interfaces;

import cl.ufro.sb_cms.model.UsuarioMetadata;

public interface UsuarioMetadataRep extends BaseRep<UsuarioMetadata>{
}
