package cl.ufro.sb_cms.repository.interfaces;

import cl.ufro.sb_cms.model.Grupo;

public interface GrupoRep extends BaseRep<Grupo>{
}
