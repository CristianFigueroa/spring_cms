package cl.ufro.sb_cms.repository.interfaces;

import cl.ufro.sb_cms.model.GrupoPermiso;

public interface GrupoPermisoRep extends BaseRep<GrupoPermiso>{
}
