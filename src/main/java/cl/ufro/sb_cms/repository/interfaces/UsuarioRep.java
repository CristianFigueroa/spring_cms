package cl.ufro.sb_cms.repository.interfaces;

import cl.ufro.sb_cms.model.Usuario;

public interface UsuarioRep extends BaseRep<Usuario>{
}
