package cl.ufro.sb_cms.repository.interfaces;

import cl.ufro.sb_cms.model.Contenido;

public interface ContenidoRep extends BaseRep<Contenido>{
}
