package cl.ufro.sb_cms.repository.interfaces;

import cl.ufro.sb_cms.model.PostMetadata;

public interface PostMetadataRep extends BaseRep<PostMetadata>{
}
