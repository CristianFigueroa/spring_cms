package cl.ufro.sb_cms.repository.interfaces;

import cl.ufro.sb_cms.model.Permiso;

public interface PermisoRep extends BaseRep<Permiso>{
}
