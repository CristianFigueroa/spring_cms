package cl.ufro.sb_cms.repository;

import cl.ufro.sb_cms.model.GrupoPermiso;
import cl.ufro.sb_cms.repository.interfaces.GrupoPermisoRep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.awt.print.Pageable;
import java.util.List;

@Repository
public class GrupoPermisoRepository implements GrupoPermisoRep {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public boolean save(GrupoPermiso object) {
        return false;
    }

    @Override
    public boolean update(GrupoPermiso object) {
        return false;
    }

    @Override
    public List<GrupoPermiso> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public GrupoPermiso findById(int id) {
        return null;
    }
}
