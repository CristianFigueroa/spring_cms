package cl.ufro.sb_cms.repository;

import cl.ufro.sb_cms.model.Usuario;
import cl.ufro.sb_cms.repository.interfaces.UsuarioRep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.awt.print.Pageable;
import java.util.List;

@Repository
public class UsuarioRepository implements UsuarioRep {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public boolean save(Usuario object) {
        return false;
    }

    @Override
    public boolean update(Usuario object) {
        return false;
    }

    @Override
    public List<Usuario> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public Usuario findById(int id) {
        return null;
    }
}
