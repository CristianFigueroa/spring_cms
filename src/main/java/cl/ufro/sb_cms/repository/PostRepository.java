package cl.ufro.sb_cms.repository;

import cl.ufro.sb_cms.model.Post;
import cl.ufro.sb_cms.repository.interfaces.PostRep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.awt.print.Pageable;
import java.util.List;

@Repository
public class PostRepository implements PostRep {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public boolean save(Post object) {
        return false;
    }

    @Override
    public boolean update(Post object) {
        return false;
    }

    @Override
    public List<Post> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public Post findById(int id) {
        return null;
    }
}
