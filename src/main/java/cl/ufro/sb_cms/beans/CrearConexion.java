package cl.ufro.sb_cms.beans;

import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

@Component
public class CrearConexion {


    @Bean(name = "dataSource")
    public DataSource mysqlDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/blog?useServerPrepStmts=true"); //nombre de la databse
        dataSource.setUsername("cristian"); //usuario
        dataSource.setPassword("shippuden1"); //contraseña
        return dataSource;
    }

}
